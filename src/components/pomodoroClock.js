import React, { Component } from 'react';
import './pomodoroClock.css';



class PomodoroClock extends Component {
    state = {
        sessionLength: 5,
        sessionMin: 5,
        sessionSec: 0,
        
        breakLength: 5,
        breakMin: 5,
        breakSec: 0,
        
        session: true,
        start: false,
        counting: false,
    }

    handleIncSession = () => {
        if ( !this.state.counting ) {
            let value = Math.min(60, this.state.sessionLength + 1);
            this.setState({ sessionLength: value, sessionMin: value, sessionSec: 0 });
        }
    }

    handleIncBreak = () => {
        if ( !this.state.counting ) {
            let value = Math.min(60, this.state.breakLength + 1);
            this.setState({ breakLength: value, breakMin: value, breakSec: 0 });
        }
    }

    handleDecSession = () => {
        if ( !this.state.counting ) {
            let value = Math.max(1, this.state.sessionLength - 1);
            this.setState({ sessionLength: value, sessionMin: value, sessionSec: 0});
        }
    }

    handleDecBreak = () => {
        if ( !this.state.counting ) {
            let value = Math.max(1, this.state.breakLength - 1);
            this.setState({ breakLength: value, breakMin: value, breakSec: 0 });
        }
    }


    handleReset = () => {
        clearInterval(this.myInterval);
        this.setState({ 
            sessionLength: 5,
            sessionMin: 5,
            sessionSec: 0,
            
            breakLength: 5,
            breakMin: 5,
            breakSec: 0,
            
            session: true,
            start: false,
            counting: false,
        });
        this.audio.pause();
        this.audio.currentTime = 0;
    }

    handleStartStop = () => {
        this.setState({ counting: true });
        
        if ( !this.state.start ) {
            this.setState({ start: !this.state.start })

            this.myInterval = setInterval(() => {
                const { sessionSec, sessionMin } = this.state
                const { breakSec, breakMin } = this.state
                

                if( this.state.session ) {
                    if (sessionSec > 0) {
                        this.setState(({ sessionSec }) => ({
                            sessionSec: sessionSec - 1
                        }))
                    }
                    if (sessionSec === 0) {
                        if (sessionMin === 0) {
                            this.setState({
                                sessionMin: this.state.sessionLength,
                                sessionSec: 0,
                                
                                breakMin: this.state.breakLength,
                                breakSec: 0,
                                
                                session: !this.state.session,
                                start: true,
                            });
                            this.handleAudioTrigger();
                        }
                        else {
                            this.setState(({ sessionMin }) => ({
                                sessionMin: sessionMin - 1,
                                sessionSec: 59
                            }))
                        }
                    }
                }
                
                else {
                    if (breakSec > 0) {
                        this.setState(({ breakSec }) => ({
                            breakSec: breakSec - 1
                        }))
                      }
                      if (breakSec === 0) {
                        if (breakMin === 0) {
                            this.setState({
                                sessionMin: this.state.sessionLength,
                                sessionSec: 0,
                                
                                breakMin: this.state.breakLength,
                                breakSec: 0,
                                
                                session: !this.state.session,
                                start: true,
                            });
                            this.handleAudioTrigger();
      
                        } else {
                            this.setState(({ breakMin }) => ({
                                breakMin: breakMin - 1,
                                breakSec: 59
                            }))
                        }
                    }
                }

                
            }, 300)
        }
        
        else {
            this.setState({ start: !this.state.start });
            clearInterval(this.myInterval);
        }
    }

    handleAudioTrigger = () => {
        // instead of using this way, in order to play the audio ...
        // let audio = document.querySelector('audio');
        // audio.play();

        // we can use this way after reffering to the audio inside its tag ...
        // <audio ref = { (audio) => { this.audio = audio } } />
        // Now we can use it anywhere inside the class.
        this.audio.play();
    }

    render() { 
        let sessionCounter = (this.state.sessionMin < 10 ? `0${this.state.sessionMin}` : `${this.state.sessionMin}`)
                            + ':' + 
                            (this.state.sessionSec < 10 ? `0${this.state.sessionSec}` : `${this.state.sessionSec}`);

        let breakCounter = (this.state.breakMin < 10 ? `0${this.state.breakMin}` : `${this.state.breakMin}`)
                            + ':' + 
                            (this.state.breakSec < 10 ? `0${this.state.breakSec}` : `${this.state.breakSec}`);

        return (
            <div className='pomodoro'>
                <header>
                    <h1>Pomodoro Clock</h1>
                    <p>Designed &amp; coded by 
                        <a href='https://codepen.io/Jarrous' target='_blank'>
                             Limar Jarrous
                        </a>
                    </p>
                </header>

                <div id='timer'>
                    <div id='timer-label'>{ this.state.session ? 'Session' : 'Break' }</div>
                    <div id='time-left'>
                        { this.state.session ? sessionCounter : breakCounter }
                    </div>
                </div>
                
                <div id='settings-controls'>
                    <div className='length-control'>
                        <div id='break-label'>Break Length</div>
                        <button id='break-decrement'
                                onClick={this.handleDecBreak}>
                            <i className='fa fa-arrow-down'></i>
                        </button>
                        <span id='break-length'>{this.state.breakLength}</span>
                        <button id='break-increment'
                                onClick={this.handleIncBreak}>
                            <i className='fa fa-arrow-up'></i>
                        </button>
                    </div>

                    <div className='length-control'>
                        <div id='session-label'>Session Length</div>
                        <button id='session-decrement'
                                onClick={this.handleDecSession}>
                            <i className='fa fa-arrow-down'></i>
                        </button>
                        <span id='session-length'>{this.state.sessionLength}</span>
                        <button id='session-increment'
                                onClick={this.handleIncSession}>
                            <i className='fa fa-arrow-up'></i>
                        </button>
                    </div>
                </div>

                <div id='player-controls'>
                    Player
                    <button id='start_stop'
                            onClick={this.handleStartStop}
                            title={!this.state.start ? 'Play' : 'Pause'}>
                        { (!this.state.start)
                            ? <i className='fa fa-play'></i>
                            : <i className='fa fa-pause'></i>
                        }
                    </button>
                    <button id='reset'
                            onClick={this.handleReset}
                            title='Reset'>
                        <i className='fa fa-redo-alt'></i>
                    </button>
                </div>

                <audio id="beep"
                        preload="auto"
                        src="https://raw.githubusercontent.com/freeCodeCamp/cdn/master/build/testable-projects-fcc/audio/BeepSound.wav"
                        ref={(audio) => {
                            this.audio = audio;
                        }} />
            </div>
        );
    }
}
 
export default PomodoroClock;