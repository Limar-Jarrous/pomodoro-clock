import React from 'react';
import PomodoroClock from './components/pomodoroClock';
import './App.css';


function App() {
  return (
    <div className="App">
      <PomodoroClock />
    </div>
  );
}

export default App;